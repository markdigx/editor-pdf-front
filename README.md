
## Ideia inicial do Projeto:
A idéia central desse projeto de gerador de pdf é
de automatizar, e deixar de maneira mais eficiente 
os cronogramas que são enviados paras os clientes da markdigx.

Com esse gerador, o pessoal da automação, ou mesmo qualquer pessoa
poderá criar facilmente um pdf já estilizado para o cliente final.
Coisa que hoje em dia é feito pelo setor da criação, e quando precisa 
fazer alguma alteração, nem que seja uma virgurla, demora muito.

Espero que este projeto possa contrubiur com toda a equipe da markdigx.


### `npm start ou yarn start`
irá abir [http://localhost:3000](http://localhost:3000) No seu navegador.

##  `Prototipo inicial`
https://edro989685.invisionapp.com/freehand/Pdf-prottipo-XlKLHmmUN
##  `libs`
Estou usando a biblioteca MUI  https://mui.com/pt/
pretendo fazer todo o visual da aplicação com essa biblioteca, por enquanto fiz um css na mão,
apenas para agilizar e não ficar demorando com as validações e da lib do mui, embora ela
já seja rápida, quero primeiro fazer funcioar, e depois com o tempo ir melhorando a inteface para o usuario.


