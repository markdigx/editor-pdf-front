import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import IconButton from '@mui/material/IconButton';
import SendIcon from '@mui/icons-material/Send';
import './cronograma-post.css';

function LimparForm(){
document.getElementsByTagName('form').values = '';
}

const Input = styled('input')({
  display: 'none',
});
export default function CronogramasPost() {
  return (

    <Box
    component="form"
    sx={{
      '& .MuiTextField-root': { m: 1, width: '25ch' },
      alignItens: 'center',
      justifyContent: 'center',
      width: 300,
      height: 300,
      paddingLeft: 50,
      paddingTop: 5,
    }}
    noValidate
    autoComplete="off"
  >
      <h3 >Criar cronogramas de post</h3>
    <div>
      <TextField id="outlined-search"
        label="Titulo"
        type="search"
      />
      <TextField
        id="outlined-multiline-static"
        label="Legenda"
        multiline
        rows={4}
        name='form'
      />
      <TextField id="outlined-search"
        label="Hastags"
        type="search"
      />
      <input
        accept="image/*"
        style={{ display: 'none' }}
        id="raised-button-file"
        multiple
        type="file"
      />
    </div>
    <Stack direction="row" alignItems="center" spacing={2}>
    <label htmlFor="contained-button-file">
      <Input accept="image/*" id="contained-button-file" multiple type="file" />
      <Button  component="span">
      <AddPhotoAlternateIcon />
      </Button>
    </label>
    <label htmlFor="icon-button-file">
      <Input accept="image/*" id="icon-button-file" type="file" />
      <IconButton color="primary" aria-label="upload picture" component="span">
        <AddPhotoAlternateIcon />
      </IconButton>
    </label>
  </Stack>

  <Button id="proximo" variant="contained"  onClick={LimparForm} endIcon={<SendIcon />}>
    Proxima
  </Button>
  <Button id="export" variant="contained"  endIcon={<SendIcon />}>
    exportar
  </Button>
  </Box>
  
);
}