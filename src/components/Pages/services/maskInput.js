import React from "react";
import InputMask from "react-input-mask";

const Input = (instagram) => (
  <InputMask
   mask={instagram} 
   value={instagram.value}
    onChange={instagram.onChange} />
);

export default Input;