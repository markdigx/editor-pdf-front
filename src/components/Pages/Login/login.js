import { Formik, Form, Field, ErrorMessage } from "formik";
import Container from '@mui/material/Container';
import * as yup from "yup";
import React from "react";
import '../Login/login.css';
function Login() {
  const handleclickLogin = (values) => { console.log(values) }
  const validationLogin = yup.object().shape({
    email: yup
      .string()
      .email("Coloque um email válido")
      .required("Este campo é obrigatório"),
    password: yup
      .string()
      .min(8, "A senha deve ter 8 caracteres")
      .required("Este campo é obrigatório"),
  });

  return (
    <div className="wrap">
      <Container className="container">
        <h1>Coloque os seus dados para fazer o login no editor de PDF</h1>
        <Formik initialValues={{}} onSubmit={handleclickLogin}
          validationSchema={validationLogin}
        >
          <Form className="login-form">
            <div className="login-form-group">
              <Field name="email" className="form-filed"
                placeholder="Email" />
              <div className="login-form-"></div>
              <ErrorMessage
                component="span"
                name="email"
                className="form-error"
              />
            </div>
            <div className="login-form-group">
              <Field name="password" className="form-filed"
                placeholder="Senha" />

              <ErrorMessage
                component="span"
                name="password"
                className="form-error"
              />
            </div>
            <button className="button" type="submit">Entrar</button>
          </Form>
        </Formik>
      </Container>
    </div>
  );
}
export default Login;