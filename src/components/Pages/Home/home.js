import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import './home.css';
import { useState } from 'react';

export default function Home() {

  const [formValues, setFormValues] = useState([]); // useState um hook dos componentes, que recebe o valor e a função para definir o valor

  const handleInputChange = (e) => {// aaqui trata as mudanças do formulario
    const { name, value } = e.target;  // target para pegar as mudanças
    // quando os campos mudarem, eles vao mandar os valore e name e vão mandar para minha função
    //  console.log(name, value);

    setFormValues({ ...formValues, [name]: value }); // pego todos os elementos do meu formulario com nome e valor
  };
    let pdfData = [];

    const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const data = Object.fromEntries(formData);


    pdfData = [...pdfData, formValues];
    console.log(pdfData);

  };

  return (

    <Box onSubmit={handleSubmit}
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
        alignItens: 'center',
        justifyContent: 'center',
        width: 300,
        height: 300,
        paddingLeft: 50,
        paddingTop: 5,
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          onSubmit={handleInputChange}
          id="nome"
          label="Nome"
          placeholder="digite o nome do cliente"
          multiline
          name="name"
          onChange={handleInputChange}
          value={formValues.name}

        />
        <TextField
          id="email"
          label=" Email"
          placeholder="Digite email do cliente"
          multiline
          name="email"
          onChange={handleInputChange}
          value={formValues.email}


        />
        <TextField
          id="whatsapp"
          label="WhatsApp"
          placeholder="Digite o Whatsapp do cliente"
          name="whatsapp"
          onChange={handleInputChange}
          value={formValues.whatsapp}


        />
        <TextField
          id="Instagram"
          label="instagram"
          placeholder="Digite o Instagram do cliente"
          name="instagram"
          onChange={handleInputChange}
          value={formValues.instagram}


        />
      </div>
      <Button
        type="submit" variant="contained" id="btn" >Cadastrar</Button>
    </Box>
  );
}
