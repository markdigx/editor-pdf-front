import React from "react";
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from "../Pages/Login/login";
import ClientesCadastrados from "../Pages/Clientes-Cadastrados/clientes-cadastrados";
import CronogramasPost from '../Pages/Cronograma-post/cronogramas-post';
import CronogramasTemas from '../Pages/Cronograma-temas/cronograma-temas';
import CronogramasMes from '../Pages/Cronogramas-mes/cronogramas-mes';
import Home from '../Pages/Home/home';
import EditarCronograma from '../Pages/Editar-Cronograma/cronograma';

function Rotas() {
  return (
    <BrowserRouter>
      <Routes>
      <Route path='/' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/home' element={<Home />} />
        <Route path='/CronogramasMes' element={<CronogramasMes />} />
        <Route path='/CronogramasTemas' element={<CronogramasTemas />} />
        <Route path='/CronogramasPost' element={<CronogramasPost />} />
        <Route path='/ClientesCadastrados' element={<ClientesCadastrados />} />  
        <Route path='/EditarCronograma' element={<EditarCronograma />} />  
      </Routes>
    </BrowserRouter>
  );
}
export default Rotas;
//criar layout de cada pagina para as rotas  CriarCronogramas

