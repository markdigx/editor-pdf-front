import React from 'react';
import  {Sidebardata} from './sidebardata';
function menulateral() {
    return (
      <div className="menulateral">
          <ul className="menulista">
              {Sidebardata.map((val,key)=>{
                  return(
                       <li
                   key={key}
                   className="row"
                   id={window.location.pathname == val.Link ? "active" : ""} 
                   onClick={()=>{
                       window.location.pathname = val.Link;
                   }}
                   >
                       <div id="icon">{val.Icon}</div><div id="title">{val.title}</div>
                   </li>
                  )
              })}
          </ul>
  </div>
    );
  }
  export default menulateral;