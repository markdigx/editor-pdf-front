
import HomeIcon from '@mui/icons-material/Home';
import React from "react";
import GroupIcon from '@mui/icons-material/Group';
import FeedIcon from '@mui/icons-material/Feed';
import ArticleIcon from '@mui/icons-material/Article';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import EditIcon from '@mui/icons-material/Edit';

export const Sidebardata = [
{
    title:"home",
    Icon: <HomeIcon />,
    Link:"/home"
},
{
    title:"Clientes",
    Icon: < GroupIcon />,
    Link:"ClientesCadastrados"
},
{
    title:"Cronogramas de Post",
    Icon: <FeedIcon />,
    Link:"CronogramasPost"
},
{
    title:"Cronogramas de Temas",
    Icon: <ArticleIcon />,
    Link:"/CronogramasTemas"
},
{
    title:"Editar um cronograma",
    Icon: <EditIcon />,
    Link:"EditarCronograma"
},
{
    title:"Ultimos cronogramas",
    Icon: <SettingsBackupRestoreIcon />,
    Link:"/CronogramasMes"
},
]

