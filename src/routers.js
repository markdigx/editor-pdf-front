import React from "react";
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from "./components/Pages/Login/login";
import Dashboard from "./components/Pages/Dashboard/dashboard";

export default function Rotas() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' component={Login} />
        <Route path='/dashboard' component={Dashboard} />
      </Routes>
    </BrowserRouter>
  );
}
//criar layout da pagina para as rotas

