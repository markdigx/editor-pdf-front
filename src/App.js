import React from "react";
import MenuLateral from './components/Menu/menuLateral';
import Rotas from "../src/components/routers/routes";
import '../src/App.css';

function App() {
  return (
    <div className="App">
     <MenuLateral />
     <Rotas/>
</div>
  );
}
export default App;

